const nodemailer = require('nodemailer');

const sendEmail = async (email, subject, text, link) => {
    const transporter = nodemailer.createTransport ({
        service: process.env.MAILER_SERVICE,
        host: process.env.MAILER_HOST,
        port: process.env.MAILER_PORT,
        secure: false,
        auth: {
            user: process.env.MAILER_USER,
            pass: process.env.MAILER_PASSWORD
        }
    
    });

    await transporter.sendMail({
        from: process.env.MAILER_USER,
        to: email,
        subject: subject,
        text: text
        


    });

}

module.exports = sendEmail;
