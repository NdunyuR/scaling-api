require("dotenv").config({path: './.env'});
const mysql = require ("mysql");
const host1 = process.env.DB_HOST;
const user1 = process.env.DB_USER;
const password1 = process.env.DB_PASSWORD;
const db1 = process.env.DB;
    


const pool = mysql.createPool({
    host: host1,
    user: user1,
    password: password1,
    database: db1,
});
// console.log(host1)
//         console.log(user1)
//         console.log(password1)
//         console.log(db1)


pool.getConnection((err) => {
    if (err) {
        console.log("Error in the connection")
        // console.log(host1)
        // console.log(user1)
        // console.log(password1)
        // console.log(db1)

    }
    else {
        console.log(`Database Connected`)
        
    }});



module.exports = pool;
// module.exports = connection;
// module.exports.pool.getConnection = pool.getConnection;