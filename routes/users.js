require("dotenv").config({path: '../.env'})
const express = require ("express");
const router = express.Router();
const bcrypt = require ('bcrypt');
const jwt = require ("jsonwebtoken");
const alert = require("alert");
const crypto = require ("crypto");

const pool = require ('../connection.js');
const {generateToken, authenticateToken} = require ('../authenticate.js');
// const {authenticateToken} = require ('../authenticate.js')
const sendEmail = require('../mailer.js')

// const cookieParser = require('cookie-parser');
// router.use(cookieParser())



// const middleware = (res, res, next) => {
//     const {email, password} = req.body;
//     req.router.locals.emailNew = email;
//     req.router.locals.passwordNew = password;
//     next();
// }


router
.route("/register")
.get((req,res)=> {
    res.render("register")
})
.post( async(req,res)=> {
    
    // console.log('reqBodyData', req.body)
    try{
        const passwordi = req.body.password
        console.log(passwordi)
        // const salt = await bcrypt.genSaltSync()
        const hashedPassword = await bcrypt.hash(passwordi, 10)
        const namei = req.body.username
        const emaili = req.body.email
        

    // let info = req.body;
        
    const sql = "INSERT INTO heroku_0d7726e0c014323.rose_logins (username, email, password) VALUES (?, ?, ?)";
    // const sql = "INSERT INTO boya.rose_logins (username, email, password) VALUES (?, ?, ?)";
    
    const results = await pool.query(sql, [namei, emaili, hashedPassword], (err, results)=> {
        if(err) throw err;
        else {res.redirect('/users/login')
        console.log('created')
        // console.log(results)
        // console.log(JSON.stringify(hashedPassword))
    }
    })}
    catch (err){
        console.log(err)
    }
})

router
.route("/forgotPassword")
.get( (req, res)=> {
    console.log('error thing')
    // console.log(error)
    // // generateToken(res, results1)
    res.render("forgot_password")
    // console.log('forgot password')
})
.post( async(req, res) => {

    try{
    // console.log(req.cookies)
    console.log(req.body)
    const email3 = req.body.email
    console.log(email3)
    let sql = `select * from heroku_0d7726e0c014323.rose_logins where email = ?`
    // let sql = `select * from boya.rose_logins where email = ?`
    
    const query = await pool.query (sql, [email3], (err, user) => {
        console.log(user)
        if (err){
            throw err;
        }
        else if (!user[0]){
            return res.json({status:'ok'})
        }
        else if(user[0]){
            let userEmail = user[0].email
            let user_id = user[0].id
            let userToken = crypto.randomBytes(16).toString("hex")
            let expireDate = new Date(new Date().getTime() + (60 * 60 * 1000))

            let sqlToken =  "INSERT INTO heroku_0d7726e0c014323.rose_resetPasswordToken (email, token, expiration, used ) VALUES (?, ?, ?, ?)";
            // let sqlToken =  "INSERT INTO boya.rose_resetPasswordToken (email, token, expiration, used ) VALUES (?, ?, ?, ?)";

            const queryToken = pool.query (sqlToken, [userEmail, userToken, expireDate, 0], async (err, results) => {
                if (err) {
                    throw err;
                }
                else {
                    const link = `${process.env.BASE_URL}/passwordReset/id/${user_id}/token/${userToken}`;
            
                    await sendEmail (userEmail, "password reset link", link);
                    res.send("password reset link sent to your email account")
                    console.log('something')
                }
            })

        }
    }) }
    catch(err){
        console.log(err)
    }

})



router
.route('/passwordReset/id/:id/token/:token')
.get(async(req, res) => {
    try{
    const token = req.params.token
    // console.log('token', token)
    const id =req.params.id    

    let sqlValidate = "select * from heroku_0d7726e0c014323.rose_resetPasswordToken where token = ? AND used = 0";
    // let sqlValidate = "select * from boya.rose_resetPasswordToken where token = ? AND used = 0";
    const tokenValidate = await pool.query(sqlValidate, [token], async (err, results)=> {
        console.log(results)

        if (err) {
            throw err;}
        else if(!results) {
            alert("invalid/expired token")
            res.redirect('/users/login')
        }
        else{
            res.render("password_reset")

        }
    }) }
    catch(err) {console.log(err)}
})

.post(async (req, res) => {
    // const token = req.params.token
    // console.log('token in put', token)
    // const id =req.params.id    
    // console.log("url in put",req.query.user_id)

    try{
    let token = req.params.token
    let id =req.params.id
    console.log(req.params)
    let newPassword = req.body.password
    console.log(newPassword)
    let hash = await bcrypt.hash(newPassword, 10)


    let sqlValidate = "delete from heroku_0d7726e0c014323.rose_resetPasswordToken where token = ? AND used = 0";
    // let sqlValidate = "delete from boya.rose_resetPasswordToken where token = ? AND used = 0";
    const tokenValidate = pool.query(sqlValidate, [token], async (err, results)=> {
        // console.log(results)
        if (err) {
            throw err;}
    })

    
    let sql = `update heroku_0d7726e0c014323.rose_logins SET password = ? where id = ?`
    // let sql = `update boya.rose_logins SET password = ? where id = ?`

    await pool.query(sql, [hash, id], (err, user) => {
        // console.log('entered update password query')
        if (err){console.log(err)}
        else {
            alert('password changed successfully')
            res.redirect('/users/login')
        }
    })}
    catch(err){
        console.log(err)
    }
})



router
.route('/login')
.get((req, res)=> {
    res.render("login")
})
.post( async(req, res)=> {

    try{
    const password2 = req.body.password
    const name2 = req.body.username
    const email2 = req.body.email
    // console.log(req.body)    

    if (!email2 || !password2) {
        alert('error: please enter email and password')
        res.redirect("login")

    }
    else{

    let sql = `SELECT * from heroku_0d7726e0c014323.rose_logins where email = ?`
    // let sql = `SELECT * from boya.rose_logins where email = ?`

    const connection = await pool.query(sql, [email2], (err, results) => {


        if (err){
            res.send(err,'not found')
        }
        else if (!results[0]) {
                alert('error: invalid email or password')
                res.redirect("login")

            }
            else{
            // bcrypt.compare(myPlaintextPassword, hash, function(err, result) {
            // result == true) so use results from  query to get actual data
            bcrypt.compare(password2, results[0].password, (err, match)=> {
                if(err){
                    res.send(err)
                }
                else if(!match) {
                    alert('error: invalid email or password')
                    res.redirect("login")
                }
                else{
                    const results1 = JSON.parse(JSON.stringify(results))[0]
                    // console.log('inusers',results1)

                    generateToken(res, results1) 
         
                    // res.redirect('/users/id')
                }
            }) }
            })
        }}
        catch(err){
            console.log(err)
        }
})

router.get('/logout', (req, res) => {
    console.log(req.cookies)

    res.clearCookie('refreshToken', {
        sameSite: "none",
        secure: true,
      })
    console.log(req.cookies)
    res.render("login")
})

router.get('/:id', authenticateToken, (req, res) => {
    let sql = `select * from heroku_0d7726e0c014323.rose_logins where email = ?`
    // let sql = `select * from boya.rose_logins where email = ?`

    //  use index to identify which object I'm picking the username from. Otherwise I'm picking a username from 
    //  an array of objects and the array does not have a username property.
     const email = req.results1.email
    //  console.log(username)
    pool.query (sql, [email], (err, profile) => {
        if (err) {
            res.send('not here')
        }
        else {
            res.render("profile", {
                username: JSON.stringify(profile[0].username).replace(/["]/g, ''),
                email: JSON.stringify(profile[0].email).replace(/["]/g, '')
            })
            // res.send(JSON.stringify(profile[0]))
            // console.log(req.params.id)
        }
    })
    
})





module.exports = router;