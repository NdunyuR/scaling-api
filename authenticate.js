const jwt = require ("jsonwebtoken");
require("dotenv").config({path: './.env'})



// separating the refreshAccessToken function from the login generateToken function ensures that a brand new access token is generated
// When you use the generateToken function as the refresh function, the expired access token is generated causing 'jwt expired' error.
// my theory is that the problem is caused by generateToken function refrerring to previous already generated access and refresh tokens 
// instead of creating new ones

function refreshAccessToken (res, results1) {
    // console.log('for refresh', results1)

    const accessToken = jwt.sign(results1, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '10s'})
    // console.log('refreshed')

    res.cookie('accessToken', accessToken, {
        secure:false,
        httpOnly: true,
        maxAge: 10 * 1000
                    })

}

function generateToken  (res, results1) {

    // try{
            const refreshToken = jwt.sign(results1, process.env.REFRESH_TOKEN_SECRET)
            const accessToken = jwt.sign(results1, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '10s'})

            res.cookie('refreshToken', refreshToken, {
                secure:false,
                httpOnly: true
            })
            .cookie('accessToken', accessToken, {
                secure:false,
                httpOnly: true,
                maxAge: 10 * 1000
                            })

    //sometimes, you/I are/am the problem see line right below where the 'undefined accessToken' was caused by console log
       // console.log('cookie', res.cookies.accessToken)
        res.redirect('/users/:id')

// }
// catch(err){res.send('err')}
}


function authenticateToken(req, res, next){

    const accessToken = req.cookies.accessToken
    const refreshToken = req.cookies.refreshToken
    // console.log('refresh', refreshToken)
    // console.log('no access token', accessToken)

       if  (accessToken) {
        // console.log('no access token', accessToken)
        jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, (err, results1) => {
            if (err) {
                // refreshAccessToken(res, user)
                console.log('token probably expired',err)
                return res.sendStatus(403)
            }
        // console.log('user', results1)
        req.results1 = results1
        next()
        })

           }
           else {
               jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, results1) => {
               if (err) return res.sendStatus(403)
               else {refreshAccessToken(res, results1)}
           req.results1 = results1
        //    console.log('used else from no accesstoken')
           next()

           })
        }

}

// function deleteRefreshCookie (res){
//     console.log('res deleting cookie', res.cookies)
//     res.clearCookie('refreshToken', { path: '/' })

// }


module.exports = {generateToken, authenticateToken};
// module.exports = authenticateToken;